$(function(){

    $('#caracteristicas').click(function(){
        var num = $('.caracteristicas .caract').length;
        num++;

        $('<input>').attr({
            type: 'text',
            id: 'c'+num,
            name: 'c['+num+']',
            class: 'caract form-control mb-1'
        }).appendTo('.caracteristicas');
    });


    // Eliminar caracteristicas vacias
    $('.submit').click(function(e){

        $('.caracteristicas .caract').each(function(){
            if($(this).val() == ''){
                $(this).remove();
            }
        });
    });

    // Borrar Construccion
    $('.borrar').click(function(e){

        if(confirm('Desa eliminar este registro?')){
            return true;
        } else{
            return false;
        }
    });

    // Borrar foto
    $('.delete-photo').click(function(){
        if(confirm('Desea borrar esta imagen?')){
            let id = $(this).data('id');
            let token = $(this).data('token');
            var photoContainer = $(this);

            var formData = new FormData();
            formData.append('id', id);
            fetch('/borrarFoto', 
                {
                    method : 'POST',
                    headers: {'X-CSRF-TOKEN':token},
                    body : formData
                }
            ).then(response => response.json())
            .catch(error => console.error('Error:', error))
            .then(response => {
                if(response == 1){
                    photoContainer.parent().remove();
                } else {
                    alert('ocurrió un error, intente nuevamente.')
                }
            });
            
        }
    });

    // Solo letras
    $(".onlyLetters").keypress(function(e){
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-zñÑ ]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    });
});