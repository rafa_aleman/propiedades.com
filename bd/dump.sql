/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost
 Source Database       : Testing_fullstack

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : utf-8

 Date: 09/01/2019 14:56:34 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `construcciones`
-- ----------------------------
DROP TABLE IF EXISTS `construcciones`;
CREATE TABLE `construcciones` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `clave` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colonia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delegacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caracteristicas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio` decimal(10,0) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `construcciones`
-- ----------------------------
BEGIN;
INSERT INTO `construcciones` VALUES ('1', 'PCOM-XXX/1', 'Diana Cazadora', 'Paseo de la Reforma', '222', 'Reforma', 'Cuhautemoc', '19.425153718960143', '-99.17165279388428', 'asd::asdasd::13123', '123456', '2019-08-30 23:12:35', '2019-09-01 18:28:26'), ('4', 'PCOM-XXX/4', 'Fuente de Cibeles', 'Plaza Madrid', '98', 'Roma Norte', 'Cuhautemoc', '19.42001363485005', '-99.16633129119873', 'asdasd::dasdasd::asdasd', '1111', '2019-08-31 02:58:04', '2019-09-01 18:31:16'), ('7', 'PCOM-XXX/7', 'Casa de Carranza', 'Calle Río Lerma', '123', 'Cuauhtémoc', 'Cuauhtémoc', '19.431224474991648', '-99.163498878479', 'asdasd::asdasd', '555', '2019-08-31 03:12:13', '2019-09-01 18:40:20'), ('8', 'PCOM-XXX/8', 'Casa de Toño', 'Londres', '144', 'Juarez', 'Cuhautemoc', '19.424748993825258', '-99.16521549224854', 'Pozole', '333', '2019-08-31 03:15:55', '2019-09-01 18:38:42'), ('10', 'PCOM-XXX/10', 'Museo de Cera', 'Londres', '1312', 'Juárez', 'Cuhautemoc', '19.428553370288462', '-99.1571044921875', 'c1::c2', '222', '2019-08-31 20:47:19', '2019-09-01 18:34:16'), ('14', 'PCOM-XXX/14', 'Angel de la independencia', 'Paseo de la reforma', '123', 'Reforma', 'Miguel Hidalgo', '19.42705591358924', '-99.16770458221436', 'c1::c2', '12345', '2019-08-31 23:21:39', '2019-09-01 19:27:53'), ('15', 'PCOM-XXX/15', 'Jardin del Arte', 'Calle James Sullivan', '321', 'San Rafael Ticomán', 'Cuahutemoc', '19.432964716544404', '-99.16195392608643', 'caract 1::caract 2', '423523', '2019-09-01 17:39:47', '2019-09-01 17:39:47');
COMMIT;

-- ----------------------------
--  Table structure for `images`
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `construccion_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `images`
-- ----------------------------
BEGIN;
INSERT INTO `images` VALUES ('23', '14', '/images/construcciones/14_angel-de-la-independencia.jpg', 'Angle de la Independencia', '2019-08-31 23:29:11', '2019-08-31 23:29:11'), ('24', '14', 'https://fastly.4sqi.net/img/general/500x500/18258274_fLwLRTwjYcNFjwY77jH23x3c7vjXPkqgVlwmJRuRzSc.jpg', 'El Pescadito', '2019-08-31 23:21:42', '2019-08-31 23:21:42'), ('25', '14', 'https://fastly.4sqi.net/img/general/500x500/6550930_ZoQFVlrP7xFMLTo5x2xSqfFYPmX3oLl7MnzFgeYKnL0.jpg', 'EL moro', '2019-08-31 23:21:42', '2019-08-31 23:21:42'), ('26', '14', 'https://fastly.4sqi.net/img/general/500x500/200717666_2LFb68Ywgo-bTS4uCdlj-neTGOtOHBe3eOU41XzGqCE.jpg', 'Nailroom', '2019-08-31 23:21:42', '2019-08-31 23:21:42'), ('27', '14', 'https://fastly.4sqi.net/img/general/500x500/4358415_RZzLVNNg_oOwn51Z_BCkCKNHQCnjDLzUzEadrSULiPs.jpg', 'Restaurante', '2019-08-31 23:21:42', '2019-08-31 23:21:42'), ('30', '15', '/images/construcciones/15_garden-of-art-jardin.jpg', 'Jardin del Arte', '2019-09-01 17:39:47', '2019-09-01 17:39:47'), ('31', '15', 'https://fastly.4sqi.net/img/general/500x500/144375375_hU3rFhnWtrIEij1etvLBn4f43sOAGx0T6uC2--EPdBM.jpg', 'La Casa de Toño', '2019-09-01 17:39:50', '2019-09-01 17:39:50'), ('32', '15', 'https://fastly.4sqi.net/img/general/500x500/33889260_gKZYhtO7OZHFuAHg62m3ESuGhXwEq3wEFfWMRvrfrQY.jpg', 'El Pescadito', '2019-09-01 17:39:50', '2019-09-01 17:39:50'), ('33', '15', 'https://fastly.4sqi.net/img/general/500x500/13503666_-D9i6_i46OtbgM1e4HjYBizwNUZPFnK8J_qyT95Y7f4.jpg', 'María Olé', '2019-09-01 17:39:50', '2019-09-01 17:39:50'), ('34', '15', 'https://fastly.4sqi.net/img/general/500x500/160644414_Yj4WgOumJWeEJr6oNKtc1ZNki-ESLlNQYgPfUhK4_Yg.jpg', 'Mezzo Mezzo', '2019-09-01 17:39:50', '2019-09-01 17:39:50'), ('35', '15', 'https://fastly.4sqi.net/img/general/500x500/1377443_eH8cSkOupuW5q8ppPb3dSsz02cWall2VxTx-edkYCdY.jpg', 'La Federal', '2019-09-01 17:39:50', '2019-09-01 17:39:50'), ('36', '1', '/images/construcciones/1_diana.jpg', 'Diana Caadora', '2019-09-01 18:05:53', '2019-09-01 18:05:53'), ('37', '1', 'https://fastly.4sqi.net/img/general/500x500/4726785_ymySU4EqFMTkvkVxxjZBZ_VLKmsKC9Y1kp06p3d9x2A.jpg', 'The St. Regis Mexico City', '2019-09-01 18:15:21', '2019-09-01 18:15:21'), ('38', '1', 'https://fastly.4sqi.net/img/general/500x500/4726785_ymySU4EqFMTkvkVxxjZBZ_VLKmsKC9Y1kp06p3d9x2A.jpg', 'The St. Regis Mexico City', '2019-09-01 18:16:37', '2019-09-01 18:16:37'), ('39', '1', 'https://fastly.4sqi.net/img/general/500x500/6269088_yZBF2XRa8e2cHMZ9OFb1gr8WenTCm1dORbcINc94J9A.jpg', 'Estudio Millesime', '2019-09-01 18:16:37', '2019-09-01 18:16:37'), ('40', '1', 'https://fastly.4sqi.net/img/general/500x500/4654871_1PZy8ysYleCzS6xTsnBUYpgwd085JhmiyW_CnucDceg.jpg', 'Remede Spa', '2019-09-01 18:16:37', '2019-09-01 18:16:37'), ('41', '1', 'https://fastly.4sqi.net/img/general/500x500/3744791_i1iduRiSJj7asVmkVAHNqN6EZlWNS3srELacelOW3RM.jpg', 'Candela Romero Restaurante', '2019-09-01 18:16:37', '2019-09-01 18:16:37'), ('42', '4', '/images/construcciones/4_cibeles.jpg', 'Fuente de Cibeles', '2019-09-01 18:31:16', '2019-09-01 18:31:16'), ('43', '4', 'https://fastly.4sqi.net/img/general/500x500/140449646_nR5qWXT8ipf2dByex7fJlPiopGwkSSyXMWyaTkaOxA0.jpg', 'Glorieta de la Cibeles', '2019-09-01 18:31:20', '2019-09-01 18:31:20'), ('44', '4', 'https://fastly.4sqi.net/img/general/500x500/375_LY5_Q-uEsl8RdMJTwfwUQSmo8_wHda16GA6pgscVxNY.jpg', 'Contramar', '2019-09-01 18:31:20', '2019-09-01 18:31:20'), ('45', '4', 'https://fastly.4sqi.net/img/general/500x500/26170706_5uvK-DeVs-03p-RCp58lq-iVqfkmY3tQiueU2O-nxyM.jpg', 'Panadería Rosetta', '2019-09-01 18:31:20', '2019-09-01 18:31:20'), ('46', '4', 'https://fastly.4sqi.net/img/general/500x500/16750752_Dfq4xSgD05oBD6z_G41H0d4KqTT5ffNxN9_FFTnMdbc.jpg', 'Expendio Durango', '2019-09-01 18:31:20', '2019-09-01 18:31:20'), ('47', '4', 'https://fastly.4sqi.net/img/general/500x500/94693030_jFGjT5tvlqGMEQofmAlKrZQ8Go07KvCYKk8as6N2ucU.jpg', 'Cancino Pizza', '2019-09-01 18:31:20', '2019-09-01 18:31:20'), ('48', '10', '/images/construcciones/10_museo-de-cera.jpg', 'Museo de Cera', '2019-09-01 18:34:16', '2019-09-01 18:34:16'), ('49', '10', 'https://fastly.4sqi.net/img/general/500x500/24825173_ODupdw8tKg-ZAYSD9DVdrQ1BSrc3QgzZ5T16mVtf6io.jpg', 'The Back Room', '2019-09-01 18:34:19', '2019-09-01 18:34:19'), ('50', '10', 'https://fastly.4sqi.net/img/general/500x500/45372622_vylTYTqX10aVGPyF-dhDk0WYAzqp_6Ctrt1HFvonGw8.jpg', 'Teatro Milán', '2019-09-01 18:34:19', '2019-09-01 18:34:19'), ('51', '10', 'https://fastly.4sqi.net/img/general/500x500/20778027_9FZg93hsYH4Ueg_GyvsmOaoPfsgAGsxMiZnI0ZpdhBY.jpg', 'Joe’s Gelato', '2019-09-01 18:34:19', '2019-09-01 18:34:19'), ('52', '10', 'https://fastly.4sqi.net/img/general/500x500/50865574_2VkqMXntGZhBk4LQ88vs7tZXi0seDmBri3yVBRN1s1Y.jpg', 'Cicatriz', '2019-09-01 18:34:19', '2019-09-01 18:34:19'), ('53', '8', '/images/construcciones/8_casa-tonio.jpg', 'CasadeToo', '2019-09-01 18:36:54', '2019-09-01 18:36:54'), ('54', '8', 'https://fastly.4sqi.net/img/general/500x500/35115021_YFAN67ySq4ytUpnfnSPx1Rb5YaKrj5xFEhM8wVL_Uvw.jpg', 'Tierra Garat', '2019-09-01 18:36:58', '2019-09-01 18:36:58'), ('55', '8', 'https://fastly.4sqi.net/img/general/500x500/18258274_fLwLRTwjYcNFjwY77jH23x3c7vjXPkqgVlwmJRuRzSc.jpg', 'El Pescadito', '2019-09-01 18:36:58', '2019-09-01 18:36:58'), ('56', '8', 'https://fastly.4sqi.net/img/general/500x500/16634019_EMwgFcWA9hYq2AV81-mm0xoW60S4tvm43WkBRBvZjYE.jpg', 'La Casa de Toño', '2019-09-01 18:36:58', '2019-09-01 18:36:58'), ('57', '8', 'https://fastly.4sqi.net/img/general/500x500/18701779_fldAYmgOFDFVxgzP6zDnyZPuMSedgBv8o5kwYXK5uo8.jpg', 'Cafebrería El Péndulo', '2019-09-01 18:36:58', '2019-09-01 18:36:58'), ('58', '8', 'https://fastly.4sqi.net/img/general/500x500/137710914_FJDBatoH9nn_wuLd8f6sAFaQrXf2pAlcUySB7hedwWo.jpg', 'La Casa de Toño', '2019-09-01 18:36:58', '2019-09-01 18:36:58'), ('59', '7', '/images/construcciones/7_muscasacarranza.jpg', 'Casa de Carranza', '2019-09-01 18:40:20', '2019-09-01 18:40:20'), ('60', '7', 'https://fastly.4sqi.net/img/general/500x500/144375375_hU3rFhnWtrIEij1etvLBn4f43sOAGx0T6uC2--EPdBM.jpg', 'La Casa de Toño', '2019-09-01 18:40:23', '2019-09-01 18:40:23'), ('61', '7', 'https://fastly.4sqi.net/img/general/500x500/36489662_kdl1_Se3xRFHvjK4DwSC8l5TJYDxEs_ytzsK-Dx7Rvc.jpg', 'Tierra Garat', '2019-09-01 18:40:23', '2019-09-01 18:40:23'), ('62', '7', 'https://fastly.4sqi.net/img/general/500x500/529697764_mEX5xmTIoSK-bVYu3r13pgMjdY5Clz5KM2DiGX6kHMY.jpg', 'Pasteleria Susy E Hijos', '2019-09-01 18:40:23', '2019-09-01 18:40:23'), ('63', '7', 'https://fastly.4sqi.net/img/general/500x500/33889260_gKZYhtO7OZHFuAHg62m3ESuGhXwEq3wEFfWMRvrfrQY.jpg', 'El Pescadito', '2019-09-01 18:40:23', '2019-09-01 18:40:23'), ('64', '7', 'https://fastly.4sqi.net/img/general/500x500/67281252_0DT97_uMTQqdOKtDUosuAzC0qmqv6BT_R5v2qE-f9RI.jpg', 'Toki Doki Sushi', '2019-09-01 18:40:23', '2019-09-01 18:40:23');
COMMIT;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1'), ('2', '2014_10_12_100000_create_password_resets_table', '1'), ('3', '2019_08_30_031011_create_construcciones_table', '1'), ('4', '2019_08_31_021642_create_images_table', '2');
COMMIT;

-- ----------------------------
--  Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', 'Nathan', 'Smith', 'pcom@gmail.com', '$2y$10$UnpyEmbpIodyvG5hqOb8GuDJNbPjE45yyVuUZjVeAnOdl1r9ln8gm');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
