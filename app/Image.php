<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table="images";
    protected $fillable = ['construccion_id', 'url', 'descripcion'];

    public function construccion(){
        return $this->belongsTo('App\Construccion');
    }
}
