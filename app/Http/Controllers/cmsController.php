<?php

namespace App\Http\Controllers;

use App\Image;
use App\Construccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use GuzzleHttp\Client;

class cmsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /*
    /* Obtiene las construcciones con sus imagenes - para el despliegue en el map
    */
    public function show(){
        $construcciones = Construccion::with('Images')->get();
        $construcciones = json_encode($construcciones);
        return view('mapa', compact('construcciones'));
    }

    /*
    // Metodo que despliega el formulario para la creacion de Constrcciones
    */
    public function create(){
        return view('crear');
    }

    /*
    // Metodo que INSERTA una construccion
    */
    public function store(){

        // Validar datos
        $fields = request()->validate([
            'nombre' => 'required',
            'calle' => 'required',
            'numero' => 'required',
            'colonia' => 'required',
            'delegacion' => 'required',
            'lat' => 'required',
            'long' => 'required'
        ]);


        // Guardar datos
        $construccion = new Construccion;
        $construccion->clave = "PCOM-XXX/##";
        $construccion->nombre = request('nombre');
        $construccion->calle = request('calle');
        $construccion->numero = request('numero');
        $construccion->colonia = request('colonia');
        $construccion->delegacion = request('delegacion');
        $construccion->lat = request('lat');
        $construccion->long = request('long');
        $construccion->precio = request('precio');
        if(request('c') != null && sizeof(request('c')) > 0){
            $construccion->caracteristicas = implode('::', request('c'));
        }
        $construccion->save();

        // Actualizacion de la Clave
        $construccion->clave = "PCOM-XXX/" . $construccion->id;
        $construccion->save();


        // imagenes subidas
        if(request()->hasFile('image1')){
            $this->uploadAndInsertImg(request()->file('image1'), $construccion->id, $construccion->nombre);
        }
        if(request()->hasFile('image2')){
            $this->uploadAndInsertImg(request()->file('image2'), $construccion->id, $construccion->nombre);
        }

        // Obtener e insertar imgs de Foursquare
        $FSImgs = $this->getFoursquareNearByPhotos($construccion->lat, $construccion->long, 5);
        if(sizeof($FSImgs) > 0){
            $FSData = [];
            foreach ($FSImgs as $img) {
                if(!empty($img['foto'])){
                    $FSImg = new Image;
                    $FSImg->construccion_id = $construccion->id;
                    $FSImg->url = $img['foto'];
                    $FSImg->descripcion = $img['nombre'];
                    $FSImg->save();
                }
            }
        }
        return redirect()->route('editar', ['id' => $construccion->id])->with('status', 'Construccion creada exitosamente!');
    }

    public function edit($id){
        $construccion = Construccion::find($id);
        $caracteristicas = explode('::', $construccion->caracteristicas);
        $imgs = Image::where('construccion_id', '=', $id)->get();
        return view('editar', compact('construccion', 'caracteristicas', 'imgs'));
    }

    public function update($id){

        // Validar datos
        $fields = request()->validate([
            'nombre' => 'required',
            'calle' => 'required',
            'numero' => 'required',
            'colonia' => 'required',
            'delegacion' => 'required',
            'lat' => 'required',
            'long' => 'required'
        ]);

        $caracteristicas = '';
        if(request('c') != null && sizeof(request('c')) > 0){
            $caracteristicas = implode('::', request('c'));
        }

        Construccion::where('id', request('id'))
                ->update(
                    [
                        'clave' => 'PCOM-XXX/'.$id,
                        'nombre' => request('nombre'),
                        'calle' => request('calle'),
                        'numero' => request('numero'),
                        'colonia' => request('colonia'),
                        'delegacion' => request('delegacion'),
                        'lat' => request('lat'),
                        'long' => request('long'),
                        'precio' => request('precio'),
                        'caracteristicas' => $caracteristicas
                    ]
                );
        
        // imagenes
        if(request()->hasFile('image1')){
            $this->uploadAndInsertImg(request()->file('image1'), $id, request('nombre'));
        }
        if(request()->hasFile('image2')){
            $this->uploadAndInsertImg(request()->file('image2'), $id, request('nombre'));
        }

        // Obtener e insertar imgs de Foursquare
        if(request('foursquare') == 1){
            $FSImgs = $this->getFoursquareNearByPhotos(request('lat'), request('long'), 5);
            if(sizeof($FSImgs) > 0){
                $FSData = [];
                foreach ($FSImgs as $img) {
                    if(!empty($img['foto'])){
                        $FSImg = new Image;
                        $FSImg->construccion_id = $id;
                        $FSImg->url = $img['foto'];
                        $FSImg->descripcion = $img['nombre'];
                        $FSImg->save();
                    }
                }
            }
        }

        return redirect()->route('editar', ['id' => $id])->with('status', 'Construccion creada exitosamente!');
    }

    public function delete(){
        $id = request('id');
        Construccion::destroy($id);
        return redirect()->route('home')->with('status', 'Construccion eliminada exitosamente!');
    }
    
    //Borra una imagen
    public function deletePhoto(){
        $id = request('id');
        $imgPath = Image::find($id)->url;
        if(File::exists($imgPath)){
            @File::delete($imgPath);
            @unlink($imgPath);
        }
    
        echo (Image::destroy($id)) ? 1 : 0;
    }

    protected function uploadAndInsertImg($img, $construccion_id, $construccion_nombre = ''){
        // $file = request()->file('image1');
        $file = $img;
        $name = $construccion_id . '_' . $file->getClientOriginalName();
        $path = '/images/construcciones/';
        if($file->move(public_path() . $path,  $name)){
            $img = new Image;
            $img->construccion_id = $construccion_id;
            $img->descripcion = $construccion_nombre;
            $img->url = $path . $name;
            $img->save();
        }
    }



    /***** FOURSQUARE ************* */

    // Obtiene las fotos de lugares cercanos segun la latitud y longitud
    protected function getFoursquareNearByPhotos($latitude, $longitude, $limit = 10){
        $photos = [];
        $venuesIds = $this->getFoursquareVenues($latitude, $longitude, $limit);
        foreach($venuesIds as $venue){
            $photos[] = ['venueId' => $venue['id'], 'nombre' => $venue['nombre'], 'foto' => $this->getImgFromVenueId($venue['id'])];
        }

        return $photos;
    }

    // Obtiene los Venues IDs cercanos a la latitud y longitud enviada
    protected function getFoursquareVenues($latitude, $longitude, $limit = 10){
        $v = date('Ymd');
        $query = '?ll=' . $latitude . ',' . $longitude . '&client_id=' . $this->client_id . '&client_secret='.$this->client_secret.'&v='.$v;
        $venuesIds = [];
        $i = 0;

        $client = new Client([
            'base_uri' => 'https://api.foursquare.com/v2/'
        ]);
        $response = $client->request('GET', 'venues/explore' . $query);
        $venues =  json_decode( $response->getBody()->getContents() );
        foreach($venues->response->groups[0]->items as $v){
            $venuesIds[] = ['id' => $v->venue->id, 'nombre' => $v->venue->name];
            $i++;
            if($i >= $limit) break;
        }

        return $venuesIds;
    }

    // Regresa la primera foto del VenueID enviado
    protected function getImgFromVenueId($id){
        $v = date('Ymd');
        $query = $id . '/photos/?client_id=' . $this->client_id . '&client_secret='.$this->client_secret.'&v=' . $v . '&group=venue&limit=10';

        $client = new Client([
            'base_uri' => 'https://api.foursquare.com/v2/'
        ]);
        $response = $client->request('GET', 'venues/' . $query);
        $imgs =  json_decode( $response->getBody()->getContents() );

        foreach( $imgs->response->photos->items as $ph){
            return $ph->prefix . '500x500' . $ph->suffix;
            break;
        }
    }

    // CRedenciales de  Foursquare
    protected $client_id = 'INZ1K4SQCRH5AUDBROHNFDGO5HPOSLJQKVQOAEJYV5NLGJT5';
    protected $client_secret = 'VGLBBWFJASMZEG05WVDXKX4CD1RLB2MSDH3R23JL20WNQTAA';
}
