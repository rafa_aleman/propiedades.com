<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Construccion extends Model
{
    protected $table = "construcciones";
    protected $fillable = ['clave', 'nombre',' calle', 'numero', 'colonia', 'delegacion', 'lat', 'long', 'caracteristicas', 'precio'];

    public function images(){
        return $this->hasMany('App\Image');
    }
}
