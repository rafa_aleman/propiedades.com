<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstruccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construcciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clave');
            $table->string('nombre');
            $table->string('calle');
            $table->string('numero');
            $table->string('colonia');
            $table->string('delegacion');
            $table->string('lat')->nullable($value = true);
            $table->string('long')->nullable($value = true);
            $table->string('caracteristicas')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('construcciones');
    }
}
