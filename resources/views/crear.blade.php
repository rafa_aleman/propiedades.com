@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <span>:: CREAR ::</span>
                </div>

                {{ Form::open(['url' => 'crear', 'method' => 'POST', 'class'=>'p-3 pForm', 'files' => true]) }}
                    
                    @include('partials.form')    

                    <div class="form-group caracteristicas">
                        <label clas="col-form-label">Caracteristicas</label> 
                        <input type="button" value="+" class="btn btn-primary mb-2" id="caracteristicas">
                        <input type="text" name="c[1]" id="c1" class="caract form-control mb-1">
                    </div>

                    <div class="form-group">
                        <label clas="col-form-label">Imagenes</label> 
                        {{ Form::file('image1', ['class' => 'd-block mb-2', 'accept' => 'image/*']) }}
                        {{ Form::file('image2', ['class' => 'd-block mb-2', 'accept' => 'image/*']) }}
                    </div>

                    <div class="form-row justify-content-center mt-5">
                        <a href="/home"><input type="button" value="Regresar" class="btn btn-secondary mr-3"></a>
                        <input type="submit" value="Crear" class="btn btn-primary submit">
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@endsection

