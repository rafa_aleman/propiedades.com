@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <span>:: EDITAR ::</span>
                </div>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif


                {!! Form::model($construccion, ['route' => ['editar.update', $construccion], 'method' => 'PUT', 'class'=>'p-3 pForm','files' => true]) !!}
                    
                    @include('partials.form') 

                    <div class="form-group caracteristicas">
                        <label clas="col-form-label">Caracteristicas</label> 
                        <input type="button" value="+" class="btn btn-primary mb-2" id="caracteristicas">

                        @foreach ($caracteristicas as $c)
                            <input type="text" name="c[{{$loop->index+1}}]" id="c{{$loop->index+1}}" class="caract form-control mb-1" value="{{ $c }}">
                        @endforeach
                    </div>

                    <div class="form-group">
                        <label clas="col-form-label mb-5">Imagenes 
                            {{-- &nbsp;&nbsp;&nbsp; <i>( <input type="checkbox" name="foursquare" value="1">Volver a guardar imagenes de Foursquare)</i> --}}
                        </label> 
                        {{ Form::file('image1', ['class' => 'd-block mb-2', 'accept' => 'image/*']) }}
                        {{ Form::file('image2', ['class' => 'd-block mb-2', 'accept' => 'image/*']) }}

                        <div class="d-flex">
                            @foreach ($imgs as $img)
                            <div class="card d-flex align-items-md-center border-light mr-3" style="max-width: 120px">
                                <img src="{{$img->url}}" style="width: 100px;" class="my-3">
                                <div>{{$img->descripcion}}</div>
                                <input type="button" value="Eliminar" class="btn btn-danger mr-3 delete-photo" data-id="{{$img->id}}" data-token="{{ csrf_token() }}">
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-row justify-content-center mt-5">
                        <a href="/home"><input type="button" value="Regresar" class="btn btn-secondary mr-3"></a>
                        <input type="submit" value="Guardar" class="btn btn-primary submit">
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

