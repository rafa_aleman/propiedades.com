@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <span>:: Construcciones ::</span>
                    <a href="{{ route('home') }}"><button type="button" class="btn btn-primary ">Volver al listado de propieades</button></a>
                </div>

                <div id="mapCanvas" style="width: 100%; height: 600px;"></div>

                <div id="galeria-container" class="d-flex flex-wrap justify-content-between my-5 mx-2"></div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="galeria">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Galeria</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="imgGaleria text-center">
                <img src="" alt="" class="img mx-auto m-3" style="width: 90%;">
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbV1DKkXHISkispnYgDLIqgw6GkgnHef4&callback=" type="text/javascript"></script>

<script>
var markers = {!! $construcciones !!};
console.log(markers);

function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };

    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
    map.setTilt(50);
        
    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i]['lat'], markers[i]['long']);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i]['nombre']
        });
        
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                resetGalery();
                var txt = '';
                var caracteristicas = markers[i]['caracteristicas'].split('::');
                var imagenes = markers[i]['images'];
                
                if(caracteristicas.length > 0 && caracteristicas != ''){
                    txt = 'Caracteristicas: <br> <ul>';
                    caracteristicas.forEach(function(caract) {
                        txt += '<li>'+caract+'</li>';
                    });
                    txt += '</ul>';
                }

                if(imagenes.length > 0){
                    txt += `<br><button type="button" class="btn btn-primary" onclick="doGaleria(${i})">Ver galeria</button>`;
                }

                content = `
                    <h5>${markers[i]['nombre']} (${markers[i]['clave']})</h5>
                    ${markers[i]['calle']} ${markers[i]['numero']}, Col. ${markers[i]['colonia']}, Del. ${markers[i]['delegacion']}.<br>
                    <b>Precio:</b>  $${markers[i]['precio']}
                    <br><br>
                    ${txt}
                `;
                infoWindow.setContent(content);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        google.maps.event.addListener(infoWindow, 'closeclick', function() {  
            resetGalery();
        }); 

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });
    
}

// Load initialize function
google.maps.event.addDomListener(window, 'load', initMap);

function doGaleria(i) {
    var construccion = markers[i];
    // console.log(construccion);

    resetGalery();
    divs = '';
    construccion.images.forEach(function(img) {
        divs += `<img src="${img.url}" class="img-thumbnail my-2" style="max-width:200px; cursor:pointer;" onclick="doModal(this)" title="${img.descripcion}">`;
    });
    $('#galeria-container').html(divs);

    $('#galeria .modal-title').text(construccion.nombre); 
}


function doModal(img){
    $('#galeria .imgGaleria .img').attr('src', img.src);
    console.log(img.title);
    if(img.title != "null"){
        $('#galeria .modal-title').text(img.title); 
    }
    $('#galeria').modal('show');
}

function resetGalery(){
    $('#galeria .imgGaleria .img').attr('src', '');
    $('#galeria-container').html('');
}

</script>

@endsection
