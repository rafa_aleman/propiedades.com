@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <span>Construcciones</span>
                    <div>
                        <a href="{{ route('mapa') }}"><button type="button" class="btn btn-primary ">Ver mapa</button></a>
                        <a href="{{ route('crear') }}"><button type="button" class="btn btn-success ">Crear</button></a>
                    </div>
                </div>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Clave</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Direccion</th>
                            <th scope="col">Precio</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($construcciones as $c)
                        <tr>
                            <th scope="row">{{$c->id}}</th>
                            <td>{{$c->clave}}</td>
                            <td>{{$c->nombre}}</td>
                            <td>{{$c->calle}} {{$c->numero}}, {{$c->colonia}}</td>
                            <td>${{$c->precio}}</td>
                            <td>
                                <a href="/editar/{{$c->id}}">
                                    <input type="button" value="Editar" class="btn btn-secondary">
                                </a>
                            </td>
                            <td>
                                {{ Form::open(['route' => 'borrar', 'method'=>'delete']) }}
                                    <input type="submit" value="Borrar" class="btn btn-danger borrar">
                                    <input type="hidden" name="id" value="{{$c->id}}">
                                {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
