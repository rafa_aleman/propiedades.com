<div class="form-group">
        <label clas="col-form-label">Nombre de la construcción</label>
        {{  Form::text('nombre', null, ['class' => 'form-control onlyLetters', 'required']) }}
        @error('nombre')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-row">
        <div class="form-group col-md-10">
            <label for="inputCity">Calle</label>
            {{  Form::text('calle', null, ['class' => 'form-control', 'required']) }}
            @error('calle')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        
        <div class="form-group col-md-2">
            <label for="inputZip">Número</label>
            {{  Form::text('numero', null, ['class' => 'form-control', 'maxlength' => '5', 'required']) }}
            @error('numero')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputCity">Colonia</label>
            {{  Form::text('colonia', null, ['class' => 'form-control', 'required']) }}
            @error('colonia')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        
        <div class="form-group col-md-6">
            <label for="inputZip">Delegación</label>
            {{  Form::text('delegacion', null, ['class' => 'form-control', 'required']) }}
            @error('delegacion')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="inputCity">Latitud</label>
            {{  Form::text('lat', null, ['class' => 'form-control', 'required']) }}
            @error('lat')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        
        <div class="form-group col-md-4">
            <label for="long">Longitud</label>
            {{  Form::text('long', null, ['class' => 'form-control', 'required']) }}
            @error('long')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-4">
            <label for="inputCity">Precio</label>
            {{  Form::text('precio', null, ['class' => 'form-control']) }}
            @error('precio')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
</div>