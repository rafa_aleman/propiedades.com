<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/crear', 'cmsController@create')->name('crear');
Route::post('/crear', 'cmsController@store')->name('crear');

Route::get('/editar/{id}', 'cmsController@edit')->name('editar');
Route::put('/editar/{id}', 'cmsController@update')->name('editar.update');

Route::delete('/borrar', 'cmsController@delete')->name('borrar');
Route::post('/borrarFoto', 'cmsController@deletePhoto')->name('delete.photo');

Route::get('/mapa', 'cmsController@show')->name('mapa');

// Foursquare
// Route::get('/foursquare/{lat}/{long}', 'cmsController@getFoursquareVenues')->name('FSqVenues');
// Route::get('/foursquareImg/{id}', 'cmsController@getImgFromVenueId')->name('FSqVenuesImgs');